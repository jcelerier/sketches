let canvasWidth = 500;
let canvasHeight = 500;

class Point {
  constructor() {
    this.x = Math.random() * canvasWidth;
    this.y = Math.random() * canvasHeight;
    
    this.dx = Math.random() - 0.5;
    this.dy = Math.random() - 0.5;
  }

  update()
  {
    this.x += this.dx;
    this.y += this.dy;

    if(this.x < 0)
    {
      this.x = canvasWidth;
    }
    else if(this.x > canvasWidth)
    {
      this.x = 0;
    }
    
    if(this.y < 0)
    {
      this.y = canvasHeight;
    }
    else if(this.y > canvasHeight)
    {
      this.y = 0;
    }
  }
};

function distance(p1, p2) {
  return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

let numPoints = 40;
let points = [];
function setup() {
  createCanvas(canvasWidth, canvasHeight);
  for(let i = 0; i < numPoints; i++) {
    points.push(new Point());
  }
}
// https://www.openprocessing.org/sketch/147268
function draw() {
  background(20);
  stroke(180);
  strokeWeight(0.5)
  
  // Update point positions
  for(let i = 0; i < numPoints; i++) {
    points[i].update();
  }

  // For each index, find the two closest indices  
  let triangles = [];
  for(let i = 0; i < numPoints; i++) {
    let closest_1 = -1;
    let distance_1 = 99999;
    let closest_2 = -1;
    let distance_2 = 99999;
    
    for(let j = 0; j < numPoints; j++) {
      if(j == i)
        continue;
      const dist = distance(points[i], points[j]);
      if(dist < distance_1) {
        distance_2 = distance_1;
        closest_2 = closest_1;
        distance_1 = dist;
        closest_1 = j;
      }
      else if(dist < distance_2) {
        distance_2 = dist;
        closest_2 = j;
      }
    }
    triangles.push([i, closest_1, closest_2]);
  }
  
  // Draw
  for(let i = 0; i < numPoints; i++) {
    
    stroke(90 + 100 * i / numPoints);

    const triangle = triangles[i];
    
    const intens = (triangle[0] + triangle[1] + triangle[2]) * 255 / 300;
    colorMode(HSL);
    fill(50, intens /2, intens / 2, intens / 15);
    const p0 = points[triangle[0]];
    const p1 = points[triangle[1]];
    const p2 = points[triangle[2]];
   // if(i == 0) console.log(triangle);
   beginShape();
   vertex(p0.x, p0.y);
   vertex(p1.x, p1.y);
   vertex(p2.x, p2.y);
   endShape();
  }
}
