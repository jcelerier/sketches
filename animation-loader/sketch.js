function setup() {
  background(100);  
}
 
function draw() {
  // Expliquer le centrage
  const x = 50 + (50 - 20 / 2) * cos(0.1 * frameCount);
  const y = 50 + (50 - 20 / 2) * sin(0.1 * frameCount);

  // Expliquer l'exploitation de % et de abs() pour faire un comportement cyclique
  var color = Math.abs(2 * frameCount % (255 * 2) - 255);

  fill(color);
  noStroke();
  ellipse(x, y, 20, 20);

}