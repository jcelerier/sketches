function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(100);
  for(let i = 0; i < 100; i++) {
    ellipse(i * 5, i * 5, i);
  }
}
