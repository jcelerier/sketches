const w = 400, h = 400;
let theta = 0;
function setup() {
  createCanvas(w, h);
}

function arrow(x1, y1, x2, y2) {
  stroke(0);
  fill(0);
  line(x1, y1, x2, y2);
  
  {
    push();
    translate(x2, y2);
    rotate(atan2(x1-x2, y2-y1));
    triangle(0, 0, -5, -5, 5, -5);
    pop();
  }
} 


function draw() {
  
  drawingContext.setLineDash([]);
  background(255);
  // Work from the center
  translate(w/2, w/2);
  scale(0.95);
  
  
  // Put the axes in the order we know
  scale(1, -1);
  arrow(0, -h/2, 0, h/2);
  arrow(-w/2, 0, w/2, 0);
  
  stroke(0);
  strokeWeight(2);
  noFill();

  const diameter = 0.75 * w; 
  circle(0, 0, diameter);
  
  {
    let line_x1 = diameter / 2 * cos(theta);
    let line_y1 = diameter / 2 * sin(theta);
    
    arrow(0, 0, line_x1, line_y1);

    drawingContext.setLineDash([5,5]);
    line(0, line_y1, line_x1, line_y1);
    line(line_x1, 0, line_x1, line_y1);
    drawingContext.setLineDash([]);

    noFill();
    arc(0, 0, 0.1 * w, 0.1 * h, 0, theta);

    textSize(18);
    strokeWeight(1);
    noStroke();
    fill(0);
    text('θ', 25, 25);
    scale(1, -1);
    text('r * cos(θ)', line_x1 - 5, 16);
    text('r', line_x1 / 2 + 10, -line_y1 / 2 + 10)
    text('r * sin(θ)', 5, -line_y1  + 4);
    scale(1, -1);
  }
  
  theta += 0.01;  
}




