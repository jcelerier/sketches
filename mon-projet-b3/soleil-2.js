function setup() {
  createCanvas(200, 200);

  background(10);
  translate(width / 2, height / 2);
 
  stroke(255, 255, 255);
  
  circle(0, 0, 100);

  let nombreRayons = 12;
  for(let i = 0; i < nombreRayons; i++) {
    // Angle total du cercle:  2 * PI
    let r = 0.9 * width / 2;
    let theta = i * 2 * PI / nombreRayons;

    let x = r * cos(theta);
    let y = r * sin(theta);
    
    line(0, 0, x, y);
  }
}
