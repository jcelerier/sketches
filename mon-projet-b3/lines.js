function setup() {
  createCanvas(200, 200);
  background(10, 10, 10);
  stroke(255, 255, 255);

  strokeWeight(3);

  let spacing = 7;

  for(let i = 0; i < 100; i ++) {
    line(10, i*spacing, width - 10, i*spacing);
  }

  fill(255, 0, 0);
  stroke(0, 255, 0);
  // noFill();
  // noStroke();
  ellipse(100, 100, 50, 25);

}
