function setup() {
  createCanvas(200, 200);

  background(10);
  translate(width / 2, height / 2);
 
  stroke(255, 255, 255);
  
  circle(0, 0, 100);

  line(0, 0, 
       0.9 * width / 2, 0);
  line(0, 0, 
       -0.9 * width / 2, 0);
       
  line(0, 0, 
       0, 0.9 * height / 2);

  line(0, 0, 
       0.7 * width / 2, -0.7 * height / 2);
  line(0, 0, 
       0.7 * width / 2, 0.7 * height / 2);
}
