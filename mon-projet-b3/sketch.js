function soleil(x, y, nombreRayons) { 
  push();
  translate(x, y);
 
  let maCouleur = color(random(), 0.6, 0.6);
  stroke(maCouleur);
  fill(maCouleur);
  
  // Dessine les rayons
  for(let i = 0; i < nombreRayons; i++) {
    // Angle total du cercle:  2 * PI
    let r = random(0.8, 1.) * width / 2;
    let theta = i * 2 * PI / nombreRayons;

    let x = r * cos(theta);
    let y = r * sin(theta);
    
    line(0, 0, x, y);
  }
  
  // Dessine le cercle
  circle(0, 0, random(0, 100));

  pop();
}

function setup() {
  createCanvas(200, 200);
  colorMode(HSB, 360, 100, 100);
  background(254, 57, 72);
  
  soleil(width / 2, width / 2, 12);
  soleil(0, 0, 30);
  soleil(200, 0, 30);
  
}
