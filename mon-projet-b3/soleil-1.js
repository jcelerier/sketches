function setup() {
  createCanvas(200, 200);

  background(10);
  translate(width / 2, height / 2);
 
  stroke(255, 255, 255);
  
  circle(0, 0, 100);

  let nombreRayons = 12;
  for(let i = 0; i < nombreRayons; i++) {
    // Angle total du cercle:  2 * PI
    line(0, 0,  0.9 * width / 2, 0);
    rotate(2 * PI / nombreRayons);
  }
}
