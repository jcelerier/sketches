var num = 500;
var frms = 180;
var theta = 0;

let slider;
let sliderText;
function setup() {
  slider = createSlider(0, 255, 100);
  sliderText = createSpan();
  createCanvas(540, 540, WEBGL);
}

function myShape(s, f, dir, n, v) {
  if (s == 0) {
    noStroke();
  } else {
    stroke(34);
  }

  // looks good also: 
  fill(f,f,f,100);
  fill(f);
  beginShape();
  for (var i=0; i<num; i++) {
    let angle = TWO_PI/num*i;
    let minV = map(sin(dir*theta+dir*angle*3), -1, 1, 15, slider.value());
    let d = map(sin(angle*n), -1, 1, minV, slider.value()+v);
    let x = cos(angle)*d;
    let y = sin(angle)*d;
    vertex(x, y);
  }
  endShape(CLOSE);
}

function draw() {
  sliderText.elt.innerHTML = " value: " + slider.value();
  background(240);
  //translate(width/2, height/2);
  myShape(1, 34, 1, 9,0);
  myShape(0, 238, -1, 9,10);

  theta += PI / frms;
}
