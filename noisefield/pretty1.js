
let field = [];
let particles = [];

function wrap(x, min, max) {
  if(x < min) return max;
  if(x > max) return min;
  return x;
}
class Particle {
  constructor(x, y)
  {
    this.x = x;
    this.y = y;

    this.vx = 0;
    this.vy = 0;
  }

  update()
  {
    this.vx = 0;
    this.vy = 0;
    for(let vec of field) {
      let d = dist(this.x, this.y, vec.x, vec.y);
      this.vx += vec.vx / d;
      this.vy += vec.vy / d;
    }

    this.x += this.vx;
    this.y += this.vy;

    this.x = wrap(this.x, 0, width);
    this.y = wrap(this.y, 0, height);
  }

  draw()
  {
    circle(this.x, this.y, 1);
  }

}

class FieldVector {
  constructor(x, y)
  {
    this.x = x;
    this.y = y;
    this.vx = random(-1, 1);
    this.vy = random(-1, 1);
  }
}
function setup() {
  createCanvas(400, 400);
  background(0);

  for(let x = 0; x < width; x += 20)
    for(let y = 0; y < height; y += 20)
      field.push(new FieldVector(x, y));


  for(let i = 0; i < 500; i++) {
    particles.push(new Particle(random(0, width), random(0, height)));
  }

}

function draw() {
  noStroke();
  fill(255, 255, 255, 100);

  for(let part of particles) {
    part.update();
    part.draw();
  }

}
