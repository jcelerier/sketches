
let field = [];
let particles = [];

let globalHue;
function wrap(x, min, max) {
  if(x < min) return max;
  if(x > max) return min;
  return x;
}
class Particle {
  constructor(x, y)
  {
    this.x = x;
    this.y = y;

    this.vx = 0;
    this.vy = 0;


    this.hue = random(globalHue - 0.1, globalHue + 0.1);
  }

  update()
  {
    this.vx = 0;
    this.vy = 0;
    for(let vec of field) {
      let d = dist(this.x, this.y, vec.x, vec.y);
      this.vx += vec.vx * tan(d);
      this.vy += vec.vy * tan(d);
    }

    this.x += this.vx;
    this.y += this.vy;

    this.x = wrap(this.x, 0, width);
    this.y = wrap(this.y, 0, height);
  }

  draw()
  {
    let cx = this.vx;
    let cy = this.vy;
    let cz = map(cos(this.vx / this.vy), -1, 1, 0, 1);
    fill(this.hue, 0.7, cz, 0.1);
    circle(this.x, this.y, 1);
  }

}

class FieldVector {
  constructor(x, y)
  {
    this.x = x;
    this.y = y;
    this.vx = 0.1 * random(-1, 1);
    this.vy = 0.1 * random(-1, 1);
  }
}
function setup() {
  randomSeed(0);
  noiseSeed(0);
  createCanvas(1000, 1000);
  colorMode(HSB, 1, 1, 1)
  background(0);
  globalHue = random(0, 1);
/*
  for(let x = 0; x < width; x += 200)
    for(let y = 0; y < height; y += 200)
      field.push(new FieldVector(x, y));
*/
field.push(new FieldVector(0, 0));

  for(let i = 0; i < 1000; i++) {
    particles.push(new Particle(random(0, width), random(0, height)));
  }

}

function draw() {
  noStroke();

  for(let part of particles) {
    part.update();
    part.draw();
  }

}
