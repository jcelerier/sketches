let canvasWidth = 500;
let canvasHeight = 500;

class Point {
  constructor() {
    this.x = Math.random() * canvasWidth;
    this.y = Math.random() * canvasHeight;
    
    this.dx = Math.random() - 0.5;
    this.dy = Math.random() - 0.5;
  }

  update()
  {
    this.x += this.dx;
    this.y += this.dy;

    if(this.x < 0)
    {
      this.x = canvasWidth;
    }
    else if(this.x > canvasWidth)
    {
      this.x = 0;
    }
    
    if(this.y < 0)
    {
      this.y = canvasHeight;
    }
    else if(this.y > canvasHeight)
    {
      this.y = 0;
    }
  }
};

let numPoints = 100;
let points = [];
function setup() {
  createCanvas(canvasWidth, canvasHeight);
  for(let i = 0; i < numPoints; i++) {
    points.push(new Point());
  }
}
// https://www.openprocessing.org/sketch/147268
function draw() {
  background(20);
  stroke(180);
  strokeWeight(0.5)
  
  // Update point positions
  for(let i = 0; i < numPoints; i++) {
    points[i].update();
  }

  // For each index, find the two closest indices  
  let triangles = [];
  for(let i = 0; i < numPoints; i++) {
    let closest_1 = 0;
    let closest_2 = 3;
    triangles.push([i, closest_1, closest_2]);
  }
  
  // Draw
  for(let i = 0; i < numPoints; i++) {
    
    stroke(90 + 100 * i / numPoints);

    const triangle = triangles[i];
    const p0 = points[triangle[0]];
    const p1 = points[triangle[1]];
    const p2 = points[triangle[2]];
    line(p0.x, p0.y, p1.x, p1.y);
    line(p2.x, p2.y, p1.x, p1.y);
    line(p0.x, p0.y, p2.x, p2.y);
  }
}
