let canvasWidth = 500;
let canvasHeight = 500;

class Point {
  constructor() {
    this.x = Math.random() * canvasWidth;
    this.y = Math.random() * canvasHeight;
    
    this.dx = Math.random() - 0.5;
    this.dy = Math.random() - 0.5;
  }

  update()
  {
    this.x += this.dx;
    this.y += this.dy;

    if(this.x < 0)
    {
      this.x = canvasWidth;
    }
    else if(this.x > canvasWidth)
    {
      this.x = 0;
    }
    
    if(this.y < 0)
    {
      this.y = canvasHeight;
    }
    else if(this.y > canvasHeight)
    {
      this.y = 0;
    }
  }
};

let numPoints = 100;
let points = [];
function setup() {
  createCanvas(canvasWidth, canvasHeight);
  for(let i = 0; i < numPoints; i++) {
    points.push(new Point());
  }
}
// https://www.openprocessing.org/sketch/147268

function draw() {
  background(20);
  stroke(180);
  strokeWeight(0.5)
  
  // Update
  for(let i = 0; i < numPoints; i++) {
    points[i].update();
  }

  // Draw
  for(let i = 0; i < numPoints; i++) {
    
    stroke(50 + 100 * i / numPoints);
    line(points[i].x, points[i].y, points[i].y, points[i].x);
  }
}
