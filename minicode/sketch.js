setup = () => {
  createCanvas(400, 400, WEBGL);
  smooth(1);
  k=random(0,1000);
}
draw = () => {
  background(0)
  n=100*noise(k+=0.01)
  c=color(2*n)
  stroke(255);
  noFill();

  beginShape();
  for(var i = 0; i < 10; i++) {
    k = 200*noise(i)
    vertex(-k,i,k);
  }
  endShape();
}
