setup = () => {
  createCanvas(400, 400, WEBGL);
  smooth(1);
  k=random(0,1000);
}
draw = () => {
  n=127*noise(k*=0.95)
  c=color(2*n, 0, 0, 255*k)
  stroke(c);
  fill(c);
  for(let i = n; i --> 0 ; ) {
    translate(k,k);
    rotate(i);    
    box(i);
  }
  if(k < 0.00955) {
    k=random(0,100);
  }
  if(k < 0.01) {
   k=random();
  }
}
