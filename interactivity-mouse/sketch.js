function setup() {
  createCanvas(400, 400);
}
var prevMouseX = mouseX;
var prevMouseY = mouseY;
function draw() {
  
  var dx = Math.abs(mouseX - prevMouseX);
  var dy = Math.abs(mouseX - prevMouseX);
  var speed = (dx + dy) / 2;
  fill(255 - 5 * speed);
  ellipse(mouseX, mouseY, speed, speed);
  
  prevMouseX = mouseX;
  prevMouseY = mouseY;
}
