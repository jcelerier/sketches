let x = [];
let y = [];
let nombreCercles = 12;

function setup() {
  createCanvas(400, 400);

  for(let i = 0; i < nombreCercles; i++) {
    x.push(random(0, width));
    y.push(random(0, height));
  }

}

function draw() {
  background(220);
  
  for(let i = 0; i < nombreCercles; i++) {
    x[i] = x[i] + 0.3;
    y[i] = y[i] + 0.4;
  }
  
  for(let i = 0; i < nombreCercles; i++) {
    circle(x[i], y[i], 30);
  }
}
