let x_spacing = 10;
let y_spacing = 10;
function setup() {
  pixelDensity(4);
  createCanvas(400, 400);
  createLoop({duration:6, endLoop: 3, gif:{download:true, open:true}})
}

function draw() {
  //background(0);
  colorMode(HSB, 1);
  frameCount = animLoop.progress * 400;
  for(let x = 0; x < width + 10; x += x_spacing)
  {
    for(let y = 0; y < height+10; y += y_spacing)
    {
      const h = cos(0.1 * cos(x * y) * frameCount);
      const s = sin(0.1 * cos(x * y) * frameCount);
      const b = tan(0.1 * cos(x * y) * frameCount);
      stroke(cos(0.1 * cos(x * y) * frameCount));
      noStroke();
      stroke(h - 0.1,s,b);
      strokeWeight(0.1);
      fill(h,s,b);
      square(x-10, y-10, 10 * cos(x * y + 0.01 * frameCount));
    }
  }
}
