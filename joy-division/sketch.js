const curveCount = 50;
const numPoints = 50;
let array = [];
function setup() {
  createCanvas(400, 400);

  for(let i = 0; i < curveCount; i++)
  {
    let curve = [  ];
    for(let j = 0; j < numPoints; j++)
    {
      curve.push(Math.random() * 20 + 20);
    }
    array.push(curve);
  }
}

function draw() {
  background(10);
  fill(0, 0, 0,255);
  stroke(200);
  strokeWeight(1.5);

  let arr = array;
  for(let curve = 0; curve < arr.length ; curve++)
  {
    let points = arr[curve];
    // Update the curve
    // points.shift();
    // points.push(Math.random() * 20 + 20);
    
    // Draw the shape
    beginShape();
    for(let i = 0; i < numPoints; i++)
    {
      // Filter the y value according to a gauss shape
      let x = i;
      let y = points[i];

      // Note : it would be *much* more efficient to precompute this 
      // and just multiply our arrays
      let distribution = Math.exp(-(x - numPoints/2) * (x -  numPoints/2) / 80);
      
      // Add a point in the curve
      vertex( 6 * i, 200 + 3 * curve - 2 * distribution * y);
    }
    endShape();
  }
}
