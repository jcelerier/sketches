
let orig, img;
function preload() {
  orig = loadImage('image-small.jpg');
  img = loadImage('image-small.jpg');
}

function readColor(image, x, y) {
  let index = (x + y * width) * 4;
  return color(image.pixels[index], image.pixels[index+1],image.pixels[index+2])
}

function writeColor(image, x, y, red, green, blue, alpha) {
  let index = (x + y * width) * 4;
  image.pixels[index] = red;
  image.pixels[index + 1] = green;
  image.pixels[index + 2] = blue;
  image.pixels[index + 3] = alpha;
}

function setup() {
  createCanvas(img.width, img.height);
  orig.loadPixels();
  for (let y = 0; y < img.height; y++) {
    for (let x = 0; x < img.width; x++) {
      let col = readColor(orig, x, y);
      let p = 20;
      let r = int(red(col) / p) * p;
      let g = int(green(col) / p) * p;
      let b = int(blue(col) / p) * p;
      writeColor(orig, x, y, r,g,b, 255);
    }
  }
  orig.updatePixels();
}
function draw() {
  orig.loadPixels();
  img.loadPixels();
  for (let y = 0; y < img.height; y++) {
    let cols = [];

    let n =  img.width;//min(frameCount, img.width);
    for (let x = 0; x < n; x++) {
      let col = readColor(orig,x, y);
      cols.push([brightness(col),col]);
    }
    cols = cols.sort((a, b) => a[0] - b[0] );
    for (let x = 0; x < n; x++) {
      let col = readColor(orig,x, y);

      let b = brightness(col);
      let cc = cols[x][1];
      if((frameCount%b)<10)
        writeColor(img, x, y, red(cc), green(cc), blue(cc), 255);
      else
        writeColor(img, x, y, red(col), green(col), blue(col), 255);
    }
  }
  img.updatePixels();
  image(img, 0, 0);
}
