function setup() {
  createCanvas(400, 200);
  background(0);

  stroke(255, 0, 0);
  strokeWeight(5);
  
  let nombreDeLignes = 5
  for(let i = 0; i <= nombreDeLignes; i++) 
  {
    // Lignes
    
    let ligne_x0 = 0;
    let ligne_y0 = i * height / nombreDeLignes;
    let ligne_x1 = width;
    let ligne_y1 = i * height / nombreDeLignes;
   
    line(ligne_x0, ligne_y0,
      ligne_x1, ligne_y1);
    
      /*
      line(0, 
         i * height / nombreDeLignes,
         width,
         i * height / nombreDeLignes);*/

    // Colonnes
    let colonne_x0 = i * width / nombreDeLignes;
    let colonne_y0 = 0;
    let colonne_x1 = i * width / nombreDeLignes;
    let colonne_y1 = height;

    line(colonne_x0, colonne_y0,   
         colonne_x1, colonne_y1);

  }

}
