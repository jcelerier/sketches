
let img;
function preload() {
	img = loadImage('assets/yeehaw.jpeg');
}

function filt(x, y) {
  let col = img.get(x, y);
  let value = (red(col) + green(col) + blue(col)) / 3; 
	img.set(x, y, color(value,value,value));
}
function setup() {
  createCanvas(img.width, img.height);
  for(let j = 0; j < img.height; j++) {
    for(let i = 0; i < img.width; i++) {
      filt(i, j);
    }
  }
  img.updatePixels();
  image(img, 0, 0);
}