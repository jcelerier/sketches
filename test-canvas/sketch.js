
// Creating the canvas
let canvas;

function setup() {
  createCanvas(400, 400);
  canvas = createGraphics(400, 400)
}

function draw() {
  background(220);

// Drawing on it: same functions that we used previously can be used 
// as methods 
canvas.blendMode(MULTIPLY);
canvas.noFill();
canvas.stroke(random(0, 255));
canvas.circle(random(0, canvas.width), random(0, canvas.height), 100);

canvas.fill(random(0, 255));
canvas.blendMode(DIFFERENCE);

canvas.circle(mouseX, mouseY, 100);

// Render the canvas to our screen (in setup() / draw())
blendMode(SUBTRACT)
image(canvas, 0, 0);
}
