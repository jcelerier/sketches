function setup() {
  createCanvas(windowWidth, windowHeight);
}

var t = 0;
function draw() {
  background(20);
  const circleWidth = 50;
  const circleHeight = 50;
  for(var i = 0; i <= 10; i++)
  {
    for(var j = 0; j < 10; j++)
    {
      const col = i * j + 50;
      fill(col);
      stroke(col);

      const x = i / 10;
      const y = j / 10;
      ellipse(x * windowWidth, y * windowHeight, 
       circleWidth, 
       circleHeight);
        t+=0.001;
    }
  }
}


function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}