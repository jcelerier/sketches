
function setup() {
  // put setup code here
  createCanvas(1000, 1000);
  //noFill();
}

var move = 0;
function draw() {
  // put drawing code here
  //fill(230);
  //rect(0, 0, 1000, 1000);
  let my_colors = [
    20, 40, 100, 200
  ];

  for(var x = 0; x < 1000; x = x + 20)
  {
    for(var y = 0; y < 1000; y = y + 20)
    {
     var color = 3 * ((cos(move + x / 100) * cos(move + y / 100) + 1) / 2);
     
      fill(my_colors[Math.round(color)]);
      //fill(abs(cos(x / 100)) * 255, abs(sin(x / 100)) * 255, 120);
       //translate(x, y);
      // rotate(cos(2 * Math.PI * x / 1000));
      // scale(y / 100);
      push();
      translate(x, y);
      var bwob = 1 * (cos(move) + 1) / 2;
      ellipse(cos(move)*10, cos(move)*10, bwob * 20, 20 * bwob);
      pop();
    }
    
  }
  move+=0.1;
}
