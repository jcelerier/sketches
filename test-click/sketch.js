function setup() {
  createCanvas(800, 800);
  frameRate(120);
}

var t = 0;
var prev = [0, 0];
function draw() {
  background(255,255,255,5);
  var x = cos(t * cos(t)) * 300;
  var y = cos(t * sin(2 * t))* 300;
  translate(400,400);
  fill(0);

  strokeCap(ROUND);
  strokeWeight( 5*( min(10, abs(tan(t))) + 1) / 2.);
  line(prev[0], prev[1], x, y);
  prev = [x, y];
  t += 1;
}
