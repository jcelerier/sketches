let font;
function preload() {
  font = loadFont('assets/NotoSansCJKtc-Medium.otf');
}

let points;
let bounds;
let orig;
let w, h;
class Particle {

  constructor(x, y) {
    this.origX = x;
    this.origY = y;
    this.x = random(orig[0], orig[0] + w);
    this.y = random(orig[1], orig[1] + h);
  }

  update() {
    this.x -= ((this.x - this.origX)) / random(100,200);
    this.y -= ((this.y - this.origY)) / random(100,200);
    this.x += random(-1, 1);
    this.y += random(-1, 1);
  }
  draw() {
    ellipse(this.x, this.y, 1);
  }
}

let particles = [];
function setup() {
  createCanvas(800, 800);
  frameRate(5);
  background(20);

  w = 0.8 * width;
  h = 0.8 * height;
  bounds = font.textBounds('㰷', 0, 0, 10);
  orig = [ bounds.x * w / bounds.w, 
           bounds.y * h / bounds.h ];
  console.log(orig);
  points = font.textToPoints('㰷', 0, 0, 10, {
    sampleFactor: 20,
    simplifyThreshold: 0.
  });

  for(let p of points) {
    particles.push(new Particle(p.x * w / bounds.w, p.y * h / bounds.h));
  }
}

function draw() {
  if(frameCount < 1200) {
  noStroke();
  fill(50);
  blendMode(SCREEN);
  //beginShape();
  translate(0.1 * width + -orig[0], 0.1 * height + -orig[1]);
  
  for(let p of particles) {
    p.update();
    p.draw();
  }

  // sketch copy
  // perl-rename 's/\d+/sprintf("%04d",$&)/e' *.png
  // ffmpeg -framerate 60 -pattern_type glob -i "*.png"  -c:v libx264 -crf 4 -profile:v baseline -level 3.0 -pix_fmt yuv420p   out.mp4

  saveCanvas('' + frameCount, 'png');
  console.log(frameCount);
}
}