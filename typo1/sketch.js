let font;
function preload() {
  font = loadFont('assets/NotoSansCJKtc-Medium.otf');
}

let points;
let bounds;
let orig;
let w, h;
class Particle {

  constructor(x, y) {
    this.origX = x;
    this.origY = y;
    this.x = random(orig[0], orig[0] + w);
    this.y = random(orig[1], orig[1] + h);
  }

  update() {
    this.x -= ((this.x - this.origX)) / random(0,20);
    this.y -= ((this.y - this.origY)) / random(100,200);
    this.x += random(-1, 1);
    this.y += random(-1, 1);
  }
  draw() {
    ellipse(this.x, this.y, 1);
  }
}

let particles = [];
function setup() {
  createCanvas(800, 800);
  frameRate(3);
  background(20);

  w = 0.8 * width;
  h = 0.8 * height;
  bounds = font.textBounds('塟', 0, 0, 10);
  orig = [ bounds.x * w / bounds.w, 
           bounds.y * h / bounds.h ];
  console.log(orig);
  points = font.textToPoints('塟', 0, 0, 10, {
    sampleFactor: 20,
    simplifyThreshold: 0.
  });

  for(let p of points) {
    particles.push(new Particle(p.x * w / bounds.w, p.y * h / bounds.h));
  }
}

function draw() {
  if(frameCount < 1200) {
  noStroke();
  fill(200);
  blendMode(OVERLAY);
  //beginShape();
  translate(0.1 * width + -orig[0], 0.1 * height + -orig[1]);
  
  for(let p of particles) {
    p.update();
    p.draw();
  }

  saveCanvas('' + frameCount, 'png');
}
}