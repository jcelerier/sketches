let x_spacing = 10;
let y_spacing = 10;
function setup() {
  pixelDensity(4);
  createCanvas(400, 400);
  colorMode(HSB, 1,1, 1);

  background(100);
  translate(200, 200);

  const kmax = 100;
  for(let k = 0; k < kmax; k++)
  {
      const h = k/kmax;
      const s = k/kmax; 
      const b = k/kmax;
      noStroke();
      fill(h,s,b,0.5);

      // 1st method
      /*
      // Compute some r & theta. r has to increase in some way.
      let r = 0.7 * k;
      let theta = k;

      // Compute x and  with the formula
      let x = r * cos(theta);
      let y = r * sin(theta);
      square(x, y, 0.06 * k);
      */
      
      // Second method
      // Rotate from some fixed angle
      rotate(0.3); 
      
      // Translate in a straight line
      translate(0.4 * k, 0.4 * k); 
      
      // Place the square at the center of the new coordinate system 
      square(0, 0, 0.2 *k);
  }
}
