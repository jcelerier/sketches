
const width = 100;
const height = 100;
function setup() {
  createCanvas(width, height);
  
  for(var i=0;i<width;i++){
    for(var j=0;j<height;j++) {
        set(i,j, color(random(0,255),
                       random(0,255), 
                       random(0,255)));
    }
}
updatePixels();
}
const d = 1;
/*
function setPixel(x, y, c)
{
  x = int(x);
  y = int(y);
  
  for (let i = 0; i < d; i++) {
    for (let j = 0; j < d; j++) {
      // loop over
      index = 4 * ((y * d + j) * width * d + (x * d + i));
      pixels[index] = red(c);
      pixels[index+1] = green(c);
      pixels[index+2] = blue(c);
      pixels[index+3] = 255;
    }
  }
}
*/

function getPixel(x, y)
{
  index = 4 * (int(y) * width + int(x));
  return color(pixels[index], pixels[index+1], pixels[index+2]);
}
  
function setPixel(x, y, c)
{
  index = 4 * (int(y) * width + int(x));
  pixels[index] = red(c) ;
  pixels[index+1] = green(c);
  pixels[index+2] = blue(c);
  pixels[index+3] = 255;
}
  
function draw() {

  loadPixels();
  for(var i=0;i<width;i++) {
    for(var j=0;j<height;j++) {
      var currentColor = getPixel(i,j);
      if(random(['H', 'T']) == 'H') { // Toss and check if heads

        var r = random(['T', 'R', 'B', 'L']); // Pick random neighbor

        if(r == 'L')
          setPixel((i-1+width)%width,j,currentColor);
        else if (r == 'R') 
          setPixel((i+1)%width, j, currentColor);
        else if (r == 'B') 
          setPixel(i, (j+1)%height, currentColor);
        else if (r == 'T') 
          setPixel(i,(j-1+height)%height,currentColor);
      }      
    }                  
  }
  updatePixels();
}