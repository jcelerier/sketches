class Circles {
  constructor()
  {
      this.rotation = 0;
      this.color = color(1,1,1);
      this.count = 10;
      this.radius = 100;
      this.circleRadius = 10;
  }
  
  update()
  {

  }

  draw()
  {
      const theta = 2 * PI / this.count;
      
      push();
      rotate(this.rotation * frameCount);
      noStroke();
      fill(this.color);
      for(let i = 0; i < this.count; i++) {
        rotate(theta);
        
        ellipse(this.radius, this.radius, 
                this.circleRadius, this.circleRadius);
      }
      pop();
  }
}
let circles = [];

function setup() {
createCanvas(800, 800);
colorMode(HSB, 1, 1, 1);
for(let i = 0; i < 10; i++){
  circles.push(new Circles());  
}
}

function draw() {
background(0, 0, 0.2);
translate(width/2, height/2);

for(let c of circles)
{
  c.update();
  c.draw();
}

}
