class Circles {
    constructor()
    {
        this.rotation = 0;
        this.color = color(1,1,1);
        this.count = 10;
        this.radius = 100;
        this.circleRadius = 10;
    }
    update()
    {

    }

    draw()
    {
        const theta = 2 * PI / this.count;
        
        push();
        rotate(this.rotation * frameCount);
        noStroke();
        fill(this.color);
        for(let i = 0; i < this.count; i++) {
          rotate(theta);
          
          ellipse(this.radius, this.radius, 
                  this.circleRadius, this.circleRadius);
        }
        pop();
    }
}

function setup() {
  createCanvas(800, 800);
  colorMode(HSB, 1, 1, 1);
}

function draw() {
  background(0, 0, 0.2);
  translate(width/2, height/2);

  circles({
    count: 15,
    radius: 40, 
    circleRadius: 10,
    rotation: -0.001,
    color: color(0.2,0.5,1, 0.5)
  });
  
  circles({
    count: 15,
    radius: 100,
    circleRadius: 40, 
    rotation: 0.002,
    color: color(0.7,0.5,1, 0.5)
  });
}
