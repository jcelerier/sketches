function circles(options)
{
  const theta = 2 * PI / options.count;
  
  push();
  rotate(options.rotation * frameCount);
  noStroke();
  fill(options.color);
  for(let i = 0; i < options.count; i++) {
    rotate(theta);
    
    ellipse(options.radius, options.radius, 
            options.circleRadius, options.circleRadius);
  }
  pop();
}

function setup() {
  createCanvas(800, 800);
  colorMode(HSB, 1, 1, 1);
}

function draw() {
  background(0, 0, 0.2);
  translate(width/2, height/2);

  circles({
    count: 15,
    radius: 40, 
    circleRadius: 10,
    rotation: -0.001,
    color: color(0.2,0.5,1, 0.5)
  });
  
  circles({
    count: 15,
    radius: 100,
    circleRadius: 40, 
    rotation: 0.002,
    color: color(0.7,0.5,1, 0.5)
  });
}
