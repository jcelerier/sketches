let points = [];
function setup() {
  setAttributes('antialias', true);
  createCanvas(100, 100, WEBGL);
  pixelDensity(5)
}

let hue = 50;
let sat = 50;
let bri = 100;
function draw() {
  background(0);
  blendMode(DIFFERENCE);
  colorMode(HSB);
  smooth(true);
  if(mouseIsPressed) {
  
  if(points.length > 107)
    points.shift();
  }
  console.log(points.length)
  stroke(hue, sat, bri);
  fill(hue,sat,bri*0.87);
  if(points.length < 7)
  return;
  beginShape(TRIANGLE_STRIP);
  for(let point of points) 
  {
    vertex(point[0], point[1], point[2]);
    point[2] += 1;
    point[1] += random(-1, 1);
    point[0] += random(-1, 1);
  }
  endShape();
}

function mousePressed() {
  hue = random(0, 365);
  points.push([mouseX - width/2, mouseY -height / 2, -200]);
}
