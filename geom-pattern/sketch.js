

 // https://www.openprocessing.org/sketch/617085
 // https://www.openprocessing.org/sketch/469866
 
 // https://www.openprocessing.org/sketch/875291
 // https://www.openprocessing.org/sketch/941007


var nTiles = 40;
var nFrames = 128;
var phase = 0.0;
var phaseInc = 1.0 / nFrames;

function drawCurve( a, b, i, j) {
	beginShape();
	for (var x = 0; x < TAU; x += 0.01) {
		vertex(5 + 10 * a * cos(i * x + phase) , 5 + 10 * b * sin(j * x  + phase));
  }
  {
    let x = TAU;
    vertex(5 + 10 * a * cos(i * x  + phase) , 5 + 10 * b * sin(j * x  + phase));
  }
	endShape();
}

let img;
function preload() {
	img = loadImage('assets/self-big.jpg');
}
function setup() {
  createCanvas(img.width, img.height);
  pixelDensity(1);
  noFill();
  seed = 0;
  background(0);
}
function draw() {
	background(0, 0, 0, 254);
  strokeWeight(0.1);
	resetMatrix();
	randomSeed(seed);
	noiseSeed(seed);

	var w = width / nTiles;
	
	// Create border
	var thisWidth = img.width;
	var thisHeight = img.height;
	var thisScale = thisWidth / width;

	for (var y = 0; y < nTiles; y++) {
		var yPos = y / nTiles * thisHeight;
		for (var x = 0; x < nTiles; x++) {
			push();
			var xPos = x / nTiles * thisWidth;
			
			//translate(xPos + phase, yPos);
			translate(random(0, width), random(0, height))
			rotate(random(0, TAU) + phase) 
			scale(random(1, 3))

			stroke(15);
			
			drawCurve(random(1,3), random(1,3),random(1, 10), random(1, 10), phase);

			pop();
		}
	}

	loadPixels();
	img.loadPixels();
	function filter(index) {
		pixels[index] = (0.4 * pixels[index]) * img.pixels[index];
	}
	for (y = 0; y < height; y++) {
	  for (x = 0; x < width; x++) {
		let index = (x + y * width) * 4;
		filter(index);
		filter(index+1);
		filter(index+2);
	  }
	}
	updatePixels();
	phase += phaseInc;
	if(frameCount < 129)
      saveCanvas('myCanvas-' + frameCount, 'png');
}