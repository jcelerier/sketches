

 // https://www.openprocessing.org/sketch/617085
 // https://www.openprocessing.org/sketch/469866
 
 // https://www.openprocessing.org/sketch/875291
 // https://www.openprocessing.org/sketch/941007


var nTiles = 10;
var nFrames = 128;
var phase = 0.0;
var phaseInc = 1.0 / nFrames;

function drawCurve( a, b, i, j) {
	beginShape();
	for (var x = 0; x < TAU; x += 0.01) {
		vertex(5 + 10 * a * cos(i * x + phase) , 5 + 10 * b * sin(j * x  + phase));
  }
  {
    let x = TAU;
    vertex(5 + 10 * a * cos(i * x  + phase) , 5 + 10 * b * sin(j * x  + phase));
  }
	endShape();
}

function setup() {
  createCanvas(500, 500);
  pixelDensity(3);
	noFill();
	seed = 0;
}

function draw() {
  background(255);
  strokeWeight(0.3);
	resetMatrix();
	randomSeed(seed);
	noiseSeed(seed);

	var w = width / nTiles;
	
	// Create border
	var thisWidth = 600;
	var thisHeight = 600;
	var thisScale = thisWidth / width;

	for (var y = 0; y < nTiles; y++) {
		var yPos = y / nTiles * thisHeight;
		for (var x = 0; x < nTiles; x++) {
			push();
			var xPos = x / nTiles * thisWidth;
			
			translate(xPos + phase, yPos);

			stroke(random(0, 255));
			
			drawCurve(random(1,3), random(1,3),random(1, 10), random(1, 10), phase);

			pop();
		}
	}

	phase += phaseInc;
}