preload=_=>{m=loadImage(`//source.unsplash.com/${d=300}x${d}`);}
draw=_=>{
  createCanvas(d,d);
  scale(y=3);
  translate(-100, 0);
  a=[...Array(d)].map(_=>random()<.5);
  while(y++<d) {
    a=a.map((n, i) => a[i-1]^(n||a[i+1])&&!image(m,i,y,i,y,i,y,1,1));
  } 
}
