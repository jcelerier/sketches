d=50;w=10*d;
function preload() {
  I=loadImage(`https://source.unsplash.com/${d}x${d}`);
}
function setup() {
  arr=[];
  createCanvas(w,w,WEBGL);
  S=[...Array(d)].map(_=>random()<0.5);
  Y=-1;
  while(++Y<d) {
    S = S.map((n, i) => {
      let w=(S[i-1]^(n||S[i+1]));
      if(w) arr.push(i,Y,I.get(i,Y)); 
      return w;
    });
  } 
}
function draw() {
  scale(10);
  background(0);
  noStroke();
  fill(255);
  lights();
  //rotateX(k=cos(frameCount / 60));
  //rotateY(k);
  //rotateZ(k);
  translate(-d/2,-d/2,-100);
  for(let pt of arr) {
    push();
    translate(pt[0], pt[1],pt[1]);
    box(1);
    pop();
  }
  
}
