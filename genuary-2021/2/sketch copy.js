d=100;
function preload() {
  I=loadImage(`https://source.unsplash.com/${d}x${d}`);
}
function setup() {
  createCanvas(d,d,WEBGL);
}
function draw() {
  background(0);
  noStroke();
  lights();
  translate(-d/2,-d/2,0)
  
  S=[...Array(d)].map(_=>random()<0.5);
  Y=-1;
  while(++Y<d) {
    S = S.map((n, i) => {
      let w=(S[i-1]^(n||S[i+1]));
      if(w) {push();
          translate(i,Y,red(I.get(i,Y)));
        box(1);pop();
      }//image(I,i,Y, 1, 1, i, Y, 1, 1);
      return w;
    });
  } 
}
