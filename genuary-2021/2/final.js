d=300;
preload=_=>{
  I=loadImage(`//source.unsplash.com/${d}x${d}`);
}
setup=_=>{
  createCanvas(d,d);
}
draw=_=>{
  scale(3);
  translate(-100, 0);
//  blendMode(DIFFERENCE);
  S=[...Array(d)].map(_=>Math.random()<0.5);
  Y=-1;
  while(++Y<d) {
    S = S.map((n, i) => {
      w=(S[i-1]^(n||S[i+1]));
      
      w&&image(I,i,Y, i, Y, i, Y, 1, 1);
      return w;
    });
  } 
}
