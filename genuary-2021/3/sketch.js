
C=(x,y,l)=>{
  beginShape();
  stroke(l,x,x+y);
  for(k = 0; k < 5; k++) {
    curveVertex(x +  5 * noise(k^x) , y+l*k/10);
  }
  endShape();
}
// also draw
setup=_=>{
  createCanvas(320, 250);
  background(20);
  colorMode(HSB);
  strokeWeight(4);
  for(i = 21; i-->0;) {
    C(v=15+i%10*30, w=30+50 * int(i/10),l=i*random(3,20));
    C(10+v, w,l);
  }
}