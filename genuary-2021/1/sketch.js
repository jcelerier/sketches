function setup() {
  createCanvas(700, 700, WEBGL);
  background(20);
  colorMode(HSB,100);
}
k = 0, n = 10;
function draw() {
  noFill();
  for(x = k; x < n; x++)
  for(y = k; y < n; y++)
  for(z = k; z < n; z++) {
    r=x+y+z;
    stroke(v=(k*10000)%r,100*v,100);
    translate(n-x,n-y,n-z);
    rotateX(r);
    rotateY(r);
    rotateZ(r);
    sphere(3);
  }
  
  k+=0.0001;
}


// setup=()=>{createCanvas(700,700,WEBGL);background(20);colorMode(HSB,100)};k=0;n=10;draw=()=>{noFill();for(x=k;x<n;x++)for(y=k;y<n;y++)for(z=k;z<n;z++)r=x+y+z,stroke(v=1E4*k%r,100*v,100),translate(n-x,n-y,n-z),rotateX(r),rotateY(r),rotateZ(r),sphere(3);k+=1E-4};
