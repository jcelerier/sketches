function setup() {
  createCanvas(500,500);

  fill("#DDD");

  rec2 = (x,y,n) => {
    if(n>0) {
    push();
    square(0,0, n * 10);
    //rec(0,0,n)
    rec2(n, n, n -= 4);
    pop();
    }
  }
  rec = (x,y,n) => {
    if(n>0) {
     translate(x,y);
      rotate(cos(1/n));
      rec2(0, 0, 30);
      rec(n * cos(x), n * sin(y), n-=1);
    }
  }

  rec(width/2, height/2, 8);
}
