function setup() {
  createCanvas(h=500,w=500);
  background(0);

  b=30;

  fill(20);
  stroke(250);
  rect(b,b,w-2*b,h-2*b)

  x=w/2;
  y=h/2;
  k=50;
  z=vertex;
  a=beginShape;
  e=endShape;

  colorMode(HSB,360,1,1);
  stroke(hue=0,1,1)
  console.log(hue)
  noFill();
  a();
  p=[];
  [...Array(11)].map((_,i)=>{
    console.log("point,", i)
    O=random(-k,k);
    P=random(-k,k);
    x = constrain(x+O, b, w - b);
    y = constrain(y+P, b, h - b);
    z(x,y);
    p.push([O,P]);
    if(i%10==0){
      stroke(0,1,1)
      console.log(hue)
      e();
      a();
      p.splice(1).reverse().map(t=>z(x=w-t[0],y=h-t[1]));
    }

  });
  stroke(300,1,1)
  e();
}
