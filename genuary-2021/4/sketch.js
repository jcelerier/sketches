function setup() {
  createCanvas(w=500,w);
  background(20);

  b=30;
  s="foobarbaz;\ncreateCanvas(h=500,w=500)ateCanvas(h=500,w=500)ateCanvas(h=500,w=500)ateCanvas(h=500,w=500);\ncreateCanvas(h=500,w=500);";
  noStroke();
  fill(255);
  textSize(30);
  translate(b,b);
  R=0;
  for(let c of s) {
    push();
    translate(b+42 * (R % 10),int(R / 10) * 50 - b);
    for(k = 0,n=random([3,5,7,9,17,27]); k < n; k++)
    {
      rotate(2*PI/n);
      text(c,0,0);
    }
    pop();
    R++;
  }
}

// eval(s="setup=_=>{createCanvas(w=500,w),background(20),b=30,noStroke(),fill(240),textSize(30),translate(b,b),R=0;for(let t of s){for(push(),translate(b+R%10*42,50*int(R/10)-b),k=0,n=random([3,5,7,9,17,27]);k<n;k++)rotate(2*PI/n),text(t,0,0);pop(),R++}}")