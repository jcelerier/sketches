// orig:
g=(n,f)=>n&&f(...[...Array(f.length)].map(_=>random(n)))&&g(n-1,f),setup=()=>g(99,square)

// signature:
g=(n,f)=>n&&f(...[...Array(f.length)].map(_=>random(n)))&&g(n-1,f)||1;
setup=()=>{noFill();beginShape();g(10, (x,y)=>curveVertex(50+40*sin(x),50+40*cos(y)));endShape()}