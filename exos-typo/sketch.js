
let font;
let points;
let img;
let particles = [];
class Particle {
  constructor(destX, destY) {
    this.x = random(0, width);
    this.y = random(0, height);
    this.destX = destX;
    this.destY = destY;
    this.vx = 0;
    this.vy = 0;
    this.ax = 0;
    this.ay = 0;
    
    this.mass = random(1, 5);
  }

  update() 
  {
    this.ax = 0;
    this.ay = 0;

    // Force 1 : attraction de la particule vers son point
    {
      let vec = createVector(this.x - this.destX, this.y - this.destY);
      let d = vec.mag(); 
      if(d > 5)
      {
        let n = vec.normalize();
        this.ax += -500 * n.x / (d*d);
        this.ay += -500 * n.y / (d*d);
      }
      else
      {
        this.vx *= 0.1;
        this.vy *= 0.1;
      }
    } 
    
    // Force 2: répulsion de la souris
    { 
      let vec = createVector(this.x - mouseX, this.y - mouseY);
      let d = vec.mag(); 
      if(d < 5) d = 5;

      {
        let n = vec.normalize();
        this.ax += n.x * this.mass * 200 / (d*d);
        this.ay += n.y * this.mass * 200 / (d*d);
      }
    }

//    this.ax = constrain(this.ax, -2, 2);
//    this.ay = constrain(this.ay, -2, 2);
    
    this.vx += this.ax;
    this.vy += this.ay;
    this.vx *= 0.9;
    this.vy *= 0.9;
    this.x += this.vx;
    this.y += this.vy;
  }

  draw() { 
    push();
    translate(this.x, this.y);
    fill(img.get(this.x, this.y));
    circle(0, 0, 5);
    pop();
  }
}

function preload() {
  font = loadFont('assets/NotoSans-Regular.ttf');
  img = loadImage('assets/cat.jpg');
}

function setup() {
  createCanvas(img.width, img.height);
  background(0);
  points = font.textToPoints('cat', 0, 0, 400, {
    sampleFactor: 0.4,
    simplifyThreshold: 0.0
  });
  
  img.loadPixels();
  for(let pt of points) {
    particles.push(new Particle(pt.x, pt.y + 300));
  }

}

function draw() {
  blendMode(BLEND);
  background(0, 0, 0, 25);

  noStroke();
  fill("white");
  //blendMode(ADD);

  for(let part of particles) {
    part.update();
    part.draw();
  }
}